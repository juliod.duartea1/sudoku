import arcade
import random

# Dimensiones de la ventana
SCREEN_SIZE = 500

# Tamaño de la cuadrícula del Sudoku (6x6)
GRID_SIZE = 6

# Tamaño de las celdas de la cuadrícula
CELL_SIZE = SCREEN_SIZE // GRID_SIZE

# Colores disponibles
COLORS = [arcade.color.RED, arcade.color.GREEN, arcade.color.BLUE, arcade.color.YELLOW, arcade.color.PURPLE, arcade.color.ORANGE]

# Nombres de los colores para la primera fila (como guía)
COLOR_NAMES = ["Rojo", "Verde", "Azul", "Amarillo", "Morado", "Naranja"]

class ColorButton(arcade.Sprite):

    def __init__(self, color, x, y, width, height):
        super().__init__()
        self.color = color
        self.center_x = x
        self.center_y = y
        self.width = width
        self.height = height

    def on_click(self):
        return self.color

class SudokuGame(arcade.Window):

    def __init__(self, size):
        super().__init__(size, size, "Sudoku de colores")

        self.grid = [[None for _ in range(GRID_SIZE)] for _ in range(GRID_SIZE)]
        self.selected_color = COLORS[0]  # Color inicial seleccionado
        self.color_buttons = arcade.SpriteList()

        self.reset_button = arcade.SpriteList()
        reset_button = ColorButton(arcade.color.ORANGE, self.width // 2, 20, 100, 40)
        reset_button.text = "Reiniciar"
        self.reset_button.append(reset_button)

        # Añadir botones de colores
        button_width = size // len(COLORS)
        button_height = 40
        for i, color in enumerate(COLORS):
            x = i * button_width + button_width // 2
            y = size - button_height // 2
            button = ColorButton(color, x, y, button_width, button_height)
            self.color_buttons.append(button)
        self.is_completed = False

        # Cambiar el color de fondo a blanco
        arcade.set_background_color(arcade.color.WHITE)

    def on_draw(self):
        arcade.start_render()

        # Dibujar la cuadrícula del Sudoku
        for row in range(GRID_SIZE + 1):
            y = self.height - row * CELL_SIZE
            arcade.draw_line(0, y, self.width, y, arcade.color.BLACK, 2)

        for col in range(GRID_SIZE + 1):
            x = col * CELL_SIZE
            arcade.draw_line(x, 0, x, self.height, arcade.color.BLACK, 2)

        # Dibujar los nombres de los colores en la primera fila como guía
        for col, color_name in enumerate(COLOR_NAMES):
            x = col * CELL_SIZE + CELL_SIZE // 2
            y = self.height - CELL_SIZE // 2
            arcade.draw_text(color_name, x, y, arcade.color.BLACK, font_size=12, anchor_x="center")

        # Dibujar los cuadros del Sudoku
        for row in range(GRID_SIZE):
            for col in range(GRID_SIZE):
                color = self.grid[row][col]
                if color:
                    x = col * CELL_SIZE
                    y = self.height - (row + 1) * CELL_SIZE
                    arcade.draw_rectangle_filled(x + CELL_SIZE // 2, y + CELL_SIZE // 2, CELL_SIZE, CELL_SIZE, color)

        # Dibujar los botones de colores
        self.color_buttons.draw()

        # Verificar si se ha completado el Sudoku
        if self.is_completed:
            arcade.draw_text("Ganaste!!", self.width // 2, self.height // 2, arcade.color.BLACK, font_size=40, anchor_x="center")

            # Dibujar el botón de reinicio solo cuando se completa el Sudoku
            self.reset_button.draw()
            arcade.draw_text("Reiniciar", self.width // 2, 20, arcade.color.BLACK, font_size=20, anchor_x="center")

    def on_mouse_press(self, x, y, button, modifiers):
        # Verificar si se hizo clic en algún botón de color
        for button in self.color_buttons:
            if button.collides_with_point((x, y)):
                self.selected_color = button.on_click()
                break

        # Verificar si se hizo clic en el botón de reinicio
        if self.is_completed:
            for button in self.reset_button:
                if button.collides_with_point((x, y)):
                    self.reset_game()
                    return

        # Cambiar el color del cuadro correspondiente al hacer clic en la cuadrícula del Sudoku
        row = GRID_SIZE - (y // CELL_SIZE) - 1
        col = x // CELL_SIZE
        # Verificar si el color se puede colocar en esa posición
        if self.is_valid_move(row, col):
            self.grid[row][col] = self.selected_color
        # Verificar si se ha completado el Sudoku
        self.check_sudoku()

    def reset_game(self):
        # Reiniciar el juego limpiando el tablero y marcándolo como no completado
        self.grid = [[None for _ in range(GRID_SIZE)] for _ in range(GRID_SIZE)]
        self.is_completed = False

    def is_valid_move(self, row, col):
        # Verificar si el color ya está presente en la misma fila o columna
        for i in range(GRID_SIZE):
            if self.grid[row][i] == self.selected_color or self.grid[i][col] == self.selected_color:
                return False

        # Verificar si el color ya está presente en el cuadrante correspondiente
        start_row, start_col = (row // 2) * 2, (col // 3) * 3
        for i in range(start_row, start_row + 2):
            for j in range(start_col, start_col + 3):
                if self.grid[i][j] == self.selected_color:
                    return False
        return True

    def check_sudoku(self):
        # Verificar si todas las celdas están llenas
        for row in range(GRID_SIZE):
            for col in range(GRID_SIZE):
                if self.grid[row][col] is None:
                    return
        # Marcar el Sudoku como completado
        self.is_completed = True

def main():
    window = SudokuGame(SCREEN_SIZE)
    arcade.run()

if __name__ == "__main__":
    main()

