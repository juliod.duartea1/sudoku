import arcade
import random

WIDTH = 340
HEIGHT = 600
#Dimensiones de la ventana
SCREEN_SIZE = 500

# Tamaño de la cuadrícula del Sudoku
GRID_SIZE = 4

# Tamaño de las celdas de la cuadrícula
CELL_SIZE = SCREEN_SIZE // GRID_SIZE

# Colores disponibles
COLORS = [arcade.color.RED, arcade.color.GREEN, arcade.color.BLUE, arcade.color.YELLOW]

# Nombres de los colores para la primera fila (como guía)
COLOR_NAMES = ["Rojo", "Verde", "Azul", "Amarillo"]

class ColorButton(arcade.Sprite):

    def __init__(self, color, x, y, width, height):
        super().__init__()
        self.color = color
        self.center_x = x
        self.center_y = y
        self.width = width
        self.height = height

    def on_click(self):
        return self.color

class SudokuGame(arcade.Window):

    def __init__(self, size):
        super().__init__(size, size, "Sudoku de colores")

        self.grid = [[None for _ in range(GRID_SIZE)] for _ in range(GRID_SIZE)]
        self.selected_color = COLORS[0]  # Color inicial seleccionado
        self.color_buttons = arcade.SpriteList()

        self.reset_button = arcade.SpriteList()
        reset_button = ColorButton(arcade.color.ORANGE, self.width // 2, 20, 100, 40)
        reset_button.text = "Reiniciar"
        self.reset_button.append(reset_button)

        # Añadir botones de colores
        button_width = size // len(COLORS)
        button_height = 40
        for i, color in enumerate(COLORS):
            x = i * button_width + button_width // 2
            y = size - button_height // 2
            button = ColorButton(color, x, y, button_width, button_height)
            self.color_buttons.append(button)
        self.is_completed = False

        # Cambiar el color de fondo a blanco
        arcade.set_background_color(arcade.color.WHITE)

    def on_draw(self):
        arcade.start_render()

        # Dibujar la cuadrícula del Sudoku
        for row in range(GRID_SIZE + 1):
            y = self.height - row * CELL_SIZE
            arcade.draw_line(0, y, self.width, y, arcade.color.BLACK, 2)

        for col in range(GRID_SIZE + 1):
            x = col * CELL_SIZE
            arcade.draw_line(x, 0, x, self.height, arcade.color.BLACK, 2)

        # Dibujar los nombres de los colores en la primera fila como guía
        for col, color_name in enumerate(COLOR_NAMES):
            x = col * CELL_SIZE + CELL_SIZE // 2
            y = self.height - CELL_SIZE // 2
            arcade.draw_text(color_name, x, y, arcade.color.BLACK, font_size=12, anchor_x="center")

        # Dibujar los cuadros del Sudoku
        for row in range(GRID_SIZE):
            for col in range(GRID_SIZE):
                color = self.grid[row][col]
                if color:
                    x = col * CELL_SIZE
                    y = self.height - (row + 1) * CELL_SIZE
                    arcade.draw_rectangle_filled(x + CELL_SIZE // 2, y + CELL_SIZE // 2, CELL_SIZE, CELL_SIZE, color)

        # Dibujar los botones de colores
        self.color_buttons.draw()

        # Verificar si se ha completado el Sudoku
        if self.is_completed:
            arcade.draw_text("Ganaste!!", self.width // 2, self.height // 2, arcade.color.BLACK, font_size=40, anchor_x="center")

            # Dibujar el botón de reinicio solo cuando se completa el Sudoku
            self.reset_button.draw()
            arcade.draw_text("Reiniciar", self.width // 2, 20, arcade.color.BLACK, font_size=20, anchor_x="center")

    def on_mouse_press(self, x, y, button, modifiers):
        # Verificar si se hizo clic en algún botón de color
        for button in self.color_buttons:
            if button.collides_with_point((x, y)):
                self.selected_color = button.on_click()
                break

        # Verificar si se hizo clic en el botón de reinicio
        if self.is_completed:
            for button in self.reset_button:
                if button.collides_with_point((x, y)):
                    self.reset_game()
                    return

        # Cambiar el color del cuadro correspondiente al hacer clic en la cuadrícula del Sudoku
        row = GRID_SIZE - (y // CELL_SIZE) - 1
        col = x // CELL_SIZE

        # Verificar si el color se puede colocar en esa posición
        if self.is_valid_move(row, col):
            self.grid[row][col] = self.selected_color

        # Verificar si se ha completado el Sudoku
        self.check_sudoku()

    def reset_game(self):
        # Reiniciar el juego limpiando el tablero y marcándolo como no completado
        self.grid = [[None for _ in range(GRID_SIZE)] for _ in range(GRID_SIZE)]
        self.is_completed = False

    def is_valid_move(self, row, col):
        # Verificar si el color ya está presente en la misma fila o columna
        for i in range(GRID_SIZE):
            if self.grid[row][i] == self.selected_color or self.grid[i][col] == self.selected_color:
                return False

        # Verificar si el color ya está presente en el cuadrante correspondiente
        start_row, start_col = (row // 2) * 2, (col // 2) * 2
        for i in range(start_row, start_row + 2):
            for j in range(start_col, start_col + 2):
                if self.grid[i][j] == self.selected_color:
                    return False

        return True

    def check_sudoku(self):
        # Verificar si todas las celdas están llenas
        for row in range(GRID_SIZE):
            for col in range(GRID_SIZE):
                if self.grid[row][col] is None:
                    return

        # Marcar el Sudoku como completado
        self.is_completed = True

class Button:
    def __init__(self, center_x, center_y, width, height, text):
        self.center_x = center_x
        self.center_y = center_y
        self.width = width
        self.height = height
        self.text = text

    def draw(self):
        arcade.draw_rectangle_filled(self.center_x, self.center_y, self.width, self.height, arcade.color.WILD_STRAWBERRY)
        arcade.draw_text(self.text, self.center_x, self.center_y, arcade.color.WHITE, font_size=20, anchor_x="center", anchor_y="center")

    def collides_with_point(self, x, y):
        left = self.center_x - self.width / 2
        right = self.center_x + self.width / 2
        bottom = self.center_y - self.height / 2
        top = self.center_y + self.height / 2

        if left <= x <= right and bottom <= y <= top:
            return True
        return False

class MainMenu(arcade.View):
    def __init__(self):
        super().__init__()
        self.circles = []
        self.button_ingresar = None

    def on_show(self):
        arcade.set_background_color(arcade.color.BLACK)
        self.circles = []
        for _ in range(10):
            circle = {
                "x": random.uniform(0, WIDTH),
                "y": random.uniform(0, HEIGHT),
                "radius": random.uniform(10, 30),
                "color": (random.randint(0, 255), random.randint(0, 255), random.randint(0, 255)),
                "speed_x": random.uniform(-100, 100),
                "speed_y": random.uniform(-100, 100)
            }
            self.circles.append(circle)

        self.button_ingresar = Button(
            center_x=WIDTH / 2,
            center_y=HEIGHT / 2 - 100,
            width=200,
            height=50,
            text="Ingresar"
        )

    def on_draw(self):
        arcade.start_render()

        for circle in self.circles:
            arcade.draw_circle_filled(circle["x"], circle["y"], circle["radius"], circle["color"])

        arcade.draw_rectangle_filled(WIDTH / 2, HEIGHT - 100, 250, 60, arcade.color.GREEN)
        arcade.draw_text("DOKUCOLOR", WIDTH / 2, HEIGHT - 100, arcade.color.WHITE, font_size=20, anchor_x="center", anchor_y="center")
        arcade.draw_text("!Piense y diviértase!", WIDTH / 2, HEIGHT - 160, arcade.color.WHITE, font_size=16, anchor_x="center", anchor_y="center")

        self.button_ingresar.draw()

    def on_update(self, delta_time):
        for circle in self.circles:
            circle["x"] += circle["speed_x"] * delta_time
            circle["y"] += circle["speed_y"] * delta_time

            if circle["x"] < 0 or circle["x"] > WIDTH:
                circle["speed_x"] *= -1
            if circle["y"] < 0 or circle["y"] > HEIGHT:
                circle["speed_y"] *= -1

    def on_mouse_press(self, x, y, button, modifiers):
        if self.button_ingresar.collides_with_point(x, y):
            game_menu = GameMenu(WIDTH, HEIGHT)
            self.window.show_view(game_menu)

class GameMenu(arcade.View):
    def __init__(self, width, height):
        super().__init__()
        self.width = width
        self.height = height
        self.button_jugar = None
        self.button_ayuda = None

    def on_show(self):
        arcade.set_background_color(arcade.color.BLACK)

        self.button_jugar = Button(
            center_x=self.width / 2,
            center_y=self.height / 2 - 100,
            width=200,
            height=50,
            text="Jugar"
        )

        self.button_ayuda = Button(
            center_x=self.width / 2,
            center_y=self.height / 2 - 170,
            width=200,
            height=50,
            text="Ayuda"
        )

    def on_draw(self):
        arcade.start_render()
        arcade.draw_text("Menú Principal", self.width / 2, self.height - 50, arcade.color.WHITE, font_size=20, anchor_x="center", anchor_y="center")
        self.button_jugar.draw()
        self.button_ayuda.draw()

    def on_mouse_press(self, x, y, button, modifiers):
        if self.button_jugar.collides_with_point(x, y):
            difficulty_menu = DifficultyMenu(self.width, self.height)
            self.window.show_view(difficulty_menu)
        elif self.button_ayuda.collides_with_point(x, y):
            instructions_view = InstructionsView(self.width, self.height)
            self.window.show_view(instructions_view)

class DifficultyMenu(arcade.View):
    def __init__(self, width, height):
        super().__init__()
        self.width = width
        self.height = height
        self.button_facil = None
        self.button_normal = None

    def on_show(self):
        arcade.set_background_color(arcade.color.BLACK)

        self.button_facil = Button(
            center_x=self.width / 2,
            center_y=self.height / 2 - 60,
            width=230,
            height=50,
            text="Fácil - 4 colores"
        )

        self.button_normal = Button(
            center_x=self.width / 2,
            center_y=self.height / 2 - 130,
            width=230,
            height=50,
            text="Normal - 6 colores"
        )

    def on_draw(self):
        arcade.start_render()
        arcade.draw_text("Selecciona la Dificultad", self.width / 2, self.height - 90, arcade.color.WHITE, font_size=20, anchor_x="center", anchor_y="center")
        self.button_facil.draw()
        self.button_normal.draw()

    def on_mouse_press(self, x, y, button, modifiers):
        if self.button_facil.collides_with_point(x, y):
            sudoku_view = SudokuGame(SCREEN_SIZE)
            self.window.show_view(sudoku_view)
        elif self.button_normal.collides_with_point(x, y):
            sudoku_view = SudokuGame(SCREEN_SIZE)
            self.window.show_view(sudoku_view)
    
class InstructionsView(arcade.View):
    def __init__(self, width, height):
        super().__init__()
        self.width = width
        self.height = height

    def on_show(self):
        arcade.set_background_color(arcade.color.BLACK)

    def on_draw(self):
        arcade.start_render()
        arcade.draw_text("Instrucciones ", self.width / 2, self.height - 50, arcade.color.WHITE, font_size=20, anchor_x="center", anchor_y="center")
        arcade.draw_text("El Sudoku es un juego de lógica", self.width / 2, self.height / 2 + 100, arcade.color.WHITE, font_size=14, anchor_x="center", anchor_y="center")
        arcade.draw_text("en el que debes completar una", self.width / 2, self.height / 2 + 70, arcade.color.WHITE, font_size=14, anchor_x="center", anchor_y="center")
        arcade.draw_text("cuadrícula de 4x4 o 6x6 con los colores", self.width / 2, self.height / 2 + 40, arcade.color.WHITE, font_size=14, anchor_x="center", anchor_y="center")
        arcade.draw_text("correspondientes, asegurándote de que", self.width / 2, self.height / 2 + 10, arcade.color.WHITE, font_size=14, anchor_x="center", anchor_y="center")
        arcade.draw_text("no haya repeticiones en ninguna", self.width / 2, self.height / 2 - 20, arcade.color.WHITE, font_size=14, anchor_x="center", anchor_y="center")
        arcade.draw_text("fila, columna o subcuadrícula", self.width / 2, self.height / 2 - 50, arcade.color.WHITE, font_size=14, anchor_x="center", anchor_y="center")
        arcade.draw_text("de 2x2 o 4x4.", self.width / 2, self.height / 2 - 80, arcade.color.WHITE, font_size=14, anchor_x="center", anchor_y="center")
        arcade.draw_text("Da click en la pantalla para", self.width / 2, self.height / 2 - 150, arcade.color.WHITE, font_size=14, anchor_x="center", anchor_y="center")
        arcade.draw_text("volver al menú principal.", self.width / 2, self.height / 2 - 180, arcade.color.WHITE, font_size=14, anchor_x="center", anchor_y="center")

    def on_key_press(self, key, modifiers):
        main_menu = MainMenu()  # Crea una instancia de la clase MainMenu
        self.window.show_view(main_menu)  # Muestra la vista MainMenu en la ventana


def main():
    window = arcade.Window(WIDTH, HEIGHT, "DOKUCOLOR")
    main_menu = MainMenu()
    window.show_view(main_menu)
    arcade.run()

if __name__ == "__main__":
    main()
